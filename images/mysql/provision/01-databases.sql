# create databases
CREATE DATABASE IF NOT EXISTS `magento`;
CREATE DATABASE IF NOT EXISTS `macarena_magento`;
CREATE DATABASE IF NOT EXISTS `macarena_magento_co`;

GRANT ALL PRIVILEGES ON *.* TO `root`@`%`;
