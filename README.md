# Tuten Magento Docker

## Referencia Original
- https://hub.docker.com/r/markoshust/magento-php/

Si estas en linux editar archivos: /etc/sysctl.conf para el funcionamiento de elastissearch

```bash
vm.max_map_count=262144
```

#### Existing Projects

- Download the Docker Compose template:
```bash
git clone git@bitbucket.org:marcoadasilvaa2/tuten-docker-magento.git
```

- Remove existing src directory:
```bash
rm -rf src
```

- Replace with existing source code of your existing Magento instance:
```bash
git clone git@github.com:myrepo.git src
bin/magento setup:install --base-url=http://magento.localhost:8000/ --backend-frontname="admin" --admin-firstname="Super" --admin-lastname="Admin" --admin-email="soporte@tutenlabs.com" --admin-user="superadmin" --admin-password="Admin.123" --db-name="macarena_magento" --db-host="db" --db-user="macarena" --db-password="macarena" --currency=USD --session-save=files --language=es_ES --use-rewrites=1
```

- Create a DNS host entry for the site or use magento.localhost
```bash
echo "127.0.0.1 ::1 yoursite.test" | sudo tee -a /etc/hosts
```


- Start some containers, copy files ot them and then restart the containers:
```bash
docker-compose up -d
rm -rf src/vendor
bin/copytocontainer --all ## Initial copy will take a few minutes...
```

- Install composer dependencies, then copy artifacts back to the host (for debugging purposes):
```bash
bin/composer install
bin/copyfromcontainer vendor
```

# Import existing database:
```bash
bin/clinotty mysql -hdb -umagento -pmagento macarena < existing/magento.sql
```

- Update database connection details to use the above Docker MySQL credentials:

```bash
vi src/app/etc/env.php
```
Also note: creds for the MySQL server are defined at startup from env/db.env

```bash
bin/magento setup:config:set --backend-frontname="admin" --admin-firstname="Admin" --admin-lastname="Super" --admin-email="soporte@tutenlabs.com" --admin-user="superadmin" --admin-password="1234" --db-name="magento" --db-host="db" --db-user="macarena" --db-password="macarena"  
```

- Import app-specific environment settings:
```bash
bin/magento app:config:import
```

- Set base URLs to local environment URL (if not defined in env.php file):
```bash
bin/magento config:set web/unsecure/base_url http://magento.localhost:8000/
bin/magento config:set web/secure/base_url https://magento.localhost:8443/
bin/magento config:set web/cookie/cookie_domain magento.localhost
bin/magento setup:upgrade
bin/magento cache:flush
```

- if you have problem to access admin panel, maybe try:
```bash
bin/magento config:set web/unsecure/base_url https://magento.localhost:8443/
```

- Last one restart your containers
```bash
bin/restart
```

- open https://magento.localhost

## Otras operaciones frecuentes
 
- Creacion de Usuario Adminsitrador Magento

```
bin/magento admin:user:create
```

- Modo desarrollador

```
bin/magento deploy:mode:set developer
```

## Debug 

### Xdebug & VS Code

Install and enable the PHP Debug extension from the [Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug).

Otherwise, this project now automatically sets up Xdebug support with VS Code. If you wish to set this up manually, please see the [`.vscode/launch.json`](https://github.com/markshust/docker-magento/blame/master/compose/.vscode/launch.json) file.

### Xdebug & PHPStorm

1.  First, install the [Chrome Xdebug helper](https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc). After installed, right click on the Chrome icon for it and go to Options. Under IDE Key, select PHPStorm from the list and click Save.

2.  Next, enable Xdebug in the PHP-FPM container by running: `bin/xdebug enable`, the restart the docker containers (CTRL+C then `bin/start`).

3.  Then, open `PHPStorm > Preferences > Languages & Frameworks > PHP` and configure:

    * `CLI Interpreter`
        * Create a new interpreter and specify `From Docker`, and name it `markoshust/magento-php:7-2-fpm`.
        * Choose `Docker`, then select the `markoshust/magento-php:7-2-fpm` image name, and set the `PHP Executable` to `php`.

    * `Path mappings`
        * Don't do anything here as the next `Docker container` step will automatically setup a path mapping from `/var/www/html` to `./src`.

    * `Docker container`
        * Remove any pre-existing volume bindings.
        * Ensure a volume binding has been setup for Container path of `/var/www/html` mapped to the Host path of `./src`.

4. ###### Open `PHPStorm > Preferences > Languages & Frameworks > PHP > Debug` and set Debug Port to `9001`.

5. Open `PHPStorm > Preferences > Languages & Frameworks > PHP > DBGp Proxy` and set Port to `9001`.

6. Open `PHPStorm > Preferences > Languages & Frameworks > PHP > Servers` and create a new server:

    * Set Name and Host to your domain name (ex. `magento2.test`)
    * Keep port set to `80`
    * Check the Path Mappings box and map `src` to the absolute path of `/var/www/html`

7. Go to `Run > Edit Configurations` and create a new `PHP Remote Debug` configuration by clicking the plus sign and selecting it. Set the Name to your domain (ex. `magento2.test`). Check the `Filter debug connection by IDE key` checkbox, select the server you just setup, and under IDE Key enter `PHPSTORM`. This IDE Key should match the IDE Key set by the Chrome Xdebug Helper. Then click OK to finish setting up the remote debugger in PHPStorm.

8. Open up `src/pub/index.php`, and set a breakpoint near the end of the file. Go to `Run > Debug 'magento2.test'`, and open up a web browser. Ensure the Chrome Xdebug helper is enabled by clicking on it > Debug. Navigate to your Magento store URL, and Xdebug within PHPStorm should now trigger the debugger and pause at the toggled breakpoint.

## Problemas de Cache
rm -rf generated/code/* && rm -rf generated/metadata/* && rm -rf pub/static/* && rm -rf var/cache/* && rm -rf var/composer_home/* && rm -rf var/page_cache/* && rm -rf var/view_preprocessed/* && bin/magento setup:di:compile && bin/magento setup:upgrade && bin/magento setup:static:deploy -f && bin/magento setup:static:deploy es_ES -f && bin/magento c:f && bin/magento c:c
